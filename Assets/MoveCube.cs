﻿using BeardedManStudios.Forge.Logging;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCube : MoveCubeBehavior
{
    private List<Vector3> positions = new List<Vector3>();
    private int currentPoint;
    private float t = 0;
    private float speed;


    private IEnumerator Start()
    {
        yield return new WaitForSeconds(0.5f);

        if (!networkObject.IsServer)
            InvokeRepeating("SendRPCToServer", 1, 1);

        if (networkObject.IsOwner)
        {
            networkObject.UpdateInterval = 50;
            InvokeRepeating("Rand", 1, 2);
        }

        positions.Add(GameObject.Find("1").transform.position);
        positions.Add(GameObject.Find("2").transform.position);
        positions.Add(GameObject.Find("3").transform.position);
        positions.Add(GameObject.Find("4").transform.position);

        networkObject.SendRpc(RPC_HAS_INSTANTIATED, Receivers.Server);
    }

    private void SendRPCToServer()
    {
        networkObject.SendRpc(RPC_HAS_INSTANTIATED, Receivers.Server);
    }

    private void Rand()
    {
        speed = Random.Range(1f, 10f);
    }
    void Update()
    {

        if (networkObject == null) return;

        if (networkObject.IsServer && Input.GetKeyDown(KeyCode.Escape))
        {
            networkObject.SendRpc(MoveCube.RPC_CHANGE_COLOR, Receivers.AllBuffered, networkObject.Owner.NetworkId);
            networkObject.SendRpc(MoveCube.RPC_CHANGE_COLOR, Receivers.Server, networkObject.Owner.NetworkId);
        }

        if (networkObject.IsOwner)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                CancelInvoke();
                speed = 0;
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                InvokeRepeating("Rand", 1, 2);
            }


            if (t >= 1)
            {
                t = 0;
                currentPoint = increment(currentPoint);
            }

            t += Time.deltaTime * speed;

            transform.position = Vector3.Lerp(positions[currentPoint], positions[increment(currentPoint)], t);
            transform.Rotate(Vector3.up, Time.deltaTime * 10 * speed);

            networkObject.position = transform.position;
            networkObject.rotation = transform.rotation;
        }
        else
        {
            transform.position = networkObject.position;
            transform.rotation = networkObject.rotation;
        }
    }



    private int increment(int value)
    {
        if (value == positions.Count - 1)
            return 0;

        return value + 1;
    }

    public override void ChangeColor(RpcArgs args)
    {
        uint id = args.GetNext<uint>();

        BMSLog.Log("Chegou rpc com a informação: " + id.ToString());

        Material newMat = new Material(Shader.Find("Standard"));
        newMat.color = Helper.colors[id];
        GetComponent<MeshRenderer>().material = newMat;

    }

    public override void HasInstantiated(RpcArgs args)
    {
        if (networkObject.IsServer)
            networkObject.SendRpc(args.Info.SendingPlayer, true, MoveCube.RPC_CHANGE_COLOR, networkObject.Owner.NetworkId);
    }

    public override void Atirar(RpcArgs args)
    {
        throw new System.NotImplementedException();
    }
}

public static class Helper
{
    public static Color[] colors = new Color[]
    {
        Color.black,
        Color.red,
        Color.blue,
        Color.green,
        Color.magenta,
        Color.cyan,
        Color.yellow
    };
}
